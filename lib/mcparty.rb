require "mcparty/version"
require "httparty"
require "json"

class MCParty
  include HTTParty

  def initialize(apikey, config={})
    @auth = {username: 'apikey', password: apikey}
    shard = apikey.split("-").last
    self.class.base_uri "https://#{shard}.api.mailchimp.com/3.0"

    @config = config
    @config[:return_collections_as_arrays] = true
  end

  def md5(email)
    Digest::MD5.hexdigest email
  end

  def batch(operations)
    response = JSON.parse(self.class.post(
      "/batches",
      basic_auth: @auth,
      body: { operations: operations}.to_json,
      headers: { "Content-Type" => "application/json", "Accept" => "application/json"}
    ).body)
  end

  %w(get post put patch delete).each do |method|
    define_method method do |endpoint, opts={}|
      call_endpoint(method.to_sym, endpoint, opts)
    end
  end

  private

  def call_endpoint(verb, endpoint, opts={})
    config = @config.merge opts

    #drop beginning / if present
    endpoint = endpoint[1..-1] if endpoint[0] == "/"

    endpoint = endpoint.split("/")

    #turn emails into md5 hashes
    endpoint = endpoint.map {|p| p.match("@") ? md5(p) : p}

    # puts "/" + endpoint.join("/")

    response = JSON.parse(self.class.send(verb,
                                          "/" + endpoint.join("/"),
                                          basic_auth: @auth,
                                          headers: { "Content-Type" => "application/json", "Accept" => "application/json"},
                                          body:  opts[:body] ? opts[:body].to_json : nil,
                                          query: opts[:query]
                                         ).body)

    if response.key?(endpoint.first) && response[endpoint.first].is_a?(Array)
      #collection
      unless config[:show_links]
        response[endpoint.first].map do |r|
          r.tap { |t| t.delete("_links") }
        end
      end

      return config[:return_collections_as_arrays] ? response[endpoint.first] : response
    else
      response.delete("_links") unless config[:show_links]
      return response
    end
  end
end
