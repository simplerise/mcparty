# MCParty

A very basic wrapper for the MailChimp API using HTTParty. Most likely you want to use [Gibbon](https://github.com/amro/gibbon).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'mcparty'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install mcparty

## Usage

```ruby
mc = MCParty.new("xxxxxxxxxxxxxxxxxxxxxxxxxx-us1")

# Get a list of your lists
mc.get("/lists")

# Add a new member
mc.post("/lists/xxxxxxx/members", body: {email_address: "test@example.com", status: "subscribed"})

# Note that you can use any verb supported by MailChimp: get post put patch delete

# Lastly, can handle the batches endpoint by passing an array of operations
mc.batches([{method: "GET", path: "/lists/aaaaaaa/members"}, {method: "GET", path: "/lists/bbbbbb/members"}])

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

