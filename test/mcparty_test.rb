require 'test_helper'

class MCPartyTest < Minitest::Test
  def setup
    abort("===\nMust set ENV['MCPARTY'] for testing.\n===\n") unless ENV['MCPARTY']
    @mc = MCParty.new(ENV['MCPARTY'])
  end

  def test_that_it_has_a_version_number
    refute_nil MCParty::VERSION
  end

  def test_simple_get_lists
    assert @mc.get("/lists").count > 0
  end
end
