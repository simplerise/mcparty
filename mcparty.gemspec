# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'mcparty/version'

Gem::Specification.new do |spec|
  spec.name          = "mcparty"
  spec.version       = MCParty::VERSION
  spec.authors       = ["Kale Davis"]
  spec.email         = ["kale@simplerise.com"]

  spec.summary       = %q{A simple api wrapper for MailChimp using HTTParty}
  spec.homepage      = "https://gitlab.com/simplerise/mcparty"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "httparty"

  spec.add_development_dependency "bundler", "~> 1.11"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "minitest-reporters"
  spec.add_development_dependency "pry", "~> 0.10"
end
